import mongoose from 'mongoose'

mongoose.connect('mongodb://localhost:books', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
