import 'reflect-metadata'
import { ApolloServer } from 'apollo-server'
import { BookResolver } from './graphql/book/bookResolver'
import { buildSchema } from 'type-graphql'

import './database/connection'

async function init() {
  const schema = await buildSchema({
    resolvers: [BookResolver],
  })

  const server = new ApolloServer({ schema })

  server.listen({ port: 4100 }).then(({ url }) => {
    console.log(`🚀  Server running at ${url}`)
  })
}

init()
