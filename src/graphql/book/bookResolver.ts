import { Arg, Mutation, Query, Resolver } from 'type-graphql'
import { Book, BookInput } from './book'
import BookSchema from '../../models/bookSchema'

@Resolver()
class BookResolver {
  @Query(() => [Book])
  async showBook() {
    const book = await BookSchema.find()
    return book
  }

  @Mutation(() => Book)
  async insertBook(@Arg('input') input: BookInput) {
    const book = await BookSchema.create(input)

    return book
  }
}

export { BookResolver }
