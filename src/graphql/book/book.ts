import { Field, InputType, ObjectType } from 'type-graphql'

@ObjectType()
class Book {
  @Field()
  title: string

  @Field()
  description: string

  @Field()
  category: string
}

@InputType()
class BookInput {
  @Field()
  title: string

  @Field()
  description: string

  @Field()
  category: string
}

export { Book, BookInput }
