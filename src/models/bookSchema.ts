import mongoose, { Schema } from 'mongoose'

type Book = {
  title: String
  description: String
  category: String
}

const BookSchema = new Schema({
  title: String,
  description: String,
  category: String,
})

export default mongoose.model<Book>('Book', BookSchema)
